#include "Keyboard.h"
#include "Mouse.h"

const int button_pin = 2;
int prev_state = HIGH;

void setup() { // Initialize the buttons' inputs:
    pinMode(button_pin, INPUT_PULLUP);

    Mouse.begin();
    Keyboard.begin();

    Serial.println("Initializing in 5s...");
    delay(5*1000);
    Serial.println("Keyboard ready!");
} // end setup()

void loop() {
  // Use the pushbuttons to control the keyboard:
    int current_val = digitalRead(button_pin);
    if((current_val == LOW) && (prev_state == HIGH)) {
        delay(20);
        current_val = digitalRead(button_pin);
        if(current_val == LOW){
            Keyboard.print("Hello creator");
        }
    }
    prev_state = current_val;
}