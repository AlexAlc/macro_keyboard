# README #
Macro keyboard using Arduino Pro Micro (chip Atmel Atmega 32u4)
### What is this repository for? ###
Custom macro keyboard to connect via USB to the computer. Uses USB HID interface to send commands or key presses to the host device.

In this version there are only 11 buttons, which can be connected directly to the Arduino digital pins. Thus not requiring to connect them in a matrix pattern (and avoiding diodes for ghost keys)

Version 1.0
### How do I get set up? ###
Install [Arduino IDE](https://www.arduino.cc/) from the official webpage.

#### Dependencies
Libraries needed usually included in Arduino IDE:
* Keyboard.h
* Mouse.h

### Contribution guidelines ###
Everyone can contribute using pull requests

### Who do I talk to? ###
Repo owner --> AlexAlc