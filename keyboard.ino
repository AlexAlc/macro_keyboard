#include "Keyboard.h"
#include "Mouse.h"

// Input buttons definition
const int buttons[] = {2,3,4,5,6,7,8,9,10,16,14,15};
const int buttons_size = sizeof(buttons)/sizeof(buttons[0]);

bool prev_states[] = {HIGH,HIGH,HIGH,HIGH,HIGH,HIGH,HIGH,HIGH,HIGH,HIGH,HIGH,HIGH};
const int prev_states_size = sizeof(prev_states)/sizeof(prev_states[0]);

bool pushed = false;
// State machine for using different profiles
enum states_machine{Profile1, Profile2, Profile3};
enum states_machine profile = Profile1;

void setup() { // Initialize the buttons' inputs:
    for(int i = 0; i < buttons_size; i++){
        pinMode(buttons[i], INPUT_PULLUP);
    }

    Mouse.begin();
    Keyboard.begin();

    Serial.println("Initializing in 5s...");
    delay(5*1000);
    Serial.println("Keyboard ready!");

    int current_val = digitalRead(buttons[6]);
    if(current_val == LOW){
        Serial.println("Keyboard blocked :(");
        for(;;){} // Get stucked here for-ev-er!!
    }
} // end setup()

void loop() {
  // Use the pushbuttons to control the keyboard:
    for(int i = 0; i < buttons_size; i++){
        int current_val = digitalRead(buttons[i]);
        if((current_val == LOW) && (prev_states[i] == HIGH)) {
            delay(20);
            current_val = digitalRead(buttons[i]);
            if(current_val == LOW){
                switch (profile)
                {
                case Profile1:
                    switch (buttons[i]){
                    case 2:  Ctrl_D(); break;
                    case 3:  Ctrl_V(); break;
                    case 4:  Ctrl_C(); break;
                    case 5:  Ctrl_D_C(); break;
                    case 6:  Ctrl_K_U(); break;
                    case 7:  Ctrl_K_C(); break;
                    case 8:  Ctrl_S(); break;
                    case 9:  Ctrl_Z(); break;
                    case 10: Ctrl_X(); break;
                    case 16: Ctrl_Shift_P(); break;
                    case 14: Change_profile(); break;
                    case 15: Unassigned(); break;
                    default: break;
                    } // end switch-case(buttons)
                    break;
                case Profile2:
                    switch (buttons[i]){
                    case 2:  Press_key_f13(); break;
                    case 3:  Press_key_f14(); break;
                    case 4:  Press_key_f15(); break;
                    case 5:  Press_key_f16(); break;
                    case 6:  Press_key_f17(); break;
                    case 7:  Press_key_f18(); break;
                    case 8:  Press_key_f19(); break;
                    case 9:  Press_key_f20(); break;
                    case 10: Press_key_f21(); break;
                    case 16: Press_key_f22(); break;
                    case 14: Change_profile(); break;
                    case 15: Unassigned(); break;
                    default: break;
                    } // end switch-case(buttons)
                    break;
                case Profile3:
                    switch (buttons[i]){
                    case 2:  Command_example(); break;
                    case 3:  Unassigned(); break;
                    case 4:  Unassigned(); break;
                    case 5:  Unassigned(); break;
                    case 6:  Unassigned(); break;
                    case 7:  Unassigned(); break;
                    case 8:  Unassigned(); break;
                    case 9:  Unassigned(); break;
                    case 10: Unassigned(); break;
                    case 16: Unassigned(); break;
                    case 14: Change_profile(); break;
                    case 15: Unassigned(); break;
                    default: break;
                    } // end switch-case(buttons)
                    break;
                
                default:
                    break;
                }

            }
        } // end if
        prev_states[i] = current_val;
    } // end for
} // end loop

void Change_profile(){
    switch (profile)
    {
    case Profile1:
        profile = Profile2;
        break;
    case Profile2:
        profile = Profile3;
        break;
    case Profile3:
        profile = Profile1;
        break;
    
    default:
        break;
    }
}
void Command_example(){
    Keyboard.press(KEY_LEFT_GUI);
    Keyboard.press('r');
    delay(100);
    Keyboard.releaseAll();
    Keyboard.print("cmd");
    delay(100);
    Keyboard.press(KEY_RETURN);
    delay(100);
    Keyboard.releaseAll();
    Keyboard.print("notepad");
    delay(100);
    Keyboard.press(KEY_RETURN);
    delay(100);
    Keyboard.releaseAll();
    Keyboard.print("Hello friends");
}
void Ctrl_D(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('d');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_V(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('v');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_C(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('c');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_X(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('x');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_Z(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('z');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_D_C(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('d');
    delay(100);
    Keyboard.releaseAll();
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('c');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_K_U(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('k');
    Keyboard.press('u');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_K_C(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('k');
    Keyboard.press('c');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_S(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('s');
    delay(100);
    Keyboard.releaseAll();
}
void Ctrl_Shift_P(){
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press(KEY_LEFT_SHIFT);
    Keyboard.press('p');
    delay(100);
    Keyboard.releaseAll();
}
void Unassigned(){
    Keyboard.print("unassigned");
}
void Press_key_f13(){
    Keyboard.press(KEY_F13);
    Keyboard.releaseAll();
}
void Press_key_f14(){
    Keyboard.press(KEY_F14);
    Keyboard.releaseAll();
}
void Press_key_f15(){
    Keyboard.press(KEY_F15);
    Keyboard.releaseAll();
}
void Press_key_f16(){
    Keyboard.press(KEY_F16);
    Keyboard.releaseAll();
}
void Press_key_f17(){
    Keyboard.press(KEY_F17);
    Keyboard.releaseAll();
}
void Press_key_f18(){
    Keyboard.press(KEY_F18);
    Keyboard.releaseAll();
}
void Press_key_f19(){
    Keyboard.press(KEY_F19);
    Keyboard.releaseAll();
}
void Press_key_f20(){
    Keyboard.press(KEY_F20);
    Keyboard.releaseAll();
}
void Press_key_f21(){
    Keyboard.press(KEY_F21);
    Keyboard.releaseAll();
}
void Press_key_f22(){
    Keyboard.press(KEY_F22);
    Keyboard.releaseAll();
}
void Press_key_f23(){
    Keyboard.press(KEY_F23);
    Keyboard.releaseAll();
}


// //  Keyboard
// #define KEY_LEFT_CTRL       0x80
// #define KEY_LEFT_SHIFT      0x81
// #define KEY_LEFT_ALT        0x82
// #define KEY_LEFT_GUI        0x83
// #define KEY_RIGHT_CTRL      0x84
// #define KEY_RIGHT_SHIFT     0x85
// #define KEY_RIGHT_ALT       0x86
// #define KEY_RIGHT_GUI       0x87
//
// #define KEY_UP_ARROW        0xDA
// #define KEY_DOWN_ARROW      0xD9
// #define KEY_LEFT_ARROW      0xD8
// #define KEY_RIGHT_ARROW     0xD7
// #define KEY_BACKSPACE       0xB2
// #define KEY_TAB             0xB3
// #define KEY_RETURN          0xB0
// #define KEY_ESC             0xB1
// #define KEY_INSERT          0xD1
// #define KEY_DELETE          0xD4
// #define KEY_PAGE_UP         0xD3
// #define KEY_PAGE_DOWN       0xD6
// #define KEY_HOME            0xD2
// #define KEY_END             0xD5
// #define KEY_CAPS_LOCK       0xC1
// #define KEY_F1              0xC2
// #define KEY_F2              0xC3
// #define KEY_F3              0xC4
// #define KEY_F4              0xC5
// #define KEY_F5              0xC6
// #define KEY_F6              0xC7
// #define KEY_F7              0xC8
// #define KEY_F8              0xC9
// #define KEY_F9              0xCA
// #define KEY_F10             0xCB
// #define KEY_F11             0xCC
// #define KEY_F12             0xCD
// #define KEY_F13             0xF0
// #define KEY_F14             0xF1
// #define KEY_F15             0xF2
// #define KEY_F16             0xF3
// #define KEY_F17             0xF4
// #define KEY_F18             0xF5
// #define KEY_F19             0xF6
// #define KEY_F20             0xF7
// #define KEY_F21             0xF8
// #define KEY_F22             0xF9
// #define KEY_F23             0xFA
// #define KEY_F24             0xFB
